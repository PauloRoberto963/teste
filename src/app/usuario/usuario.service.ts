import { Injectable } from '@angular/core';

import { HttpParams, HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Usuario } from '../model/usuario';

@Injectable()
export class UsuarioService {

  usuarioUrl: string;

  constructor(private http: HttpClient) {
    this.usuarioUrl = `${environment.apiUrl}`;
  }

  listarTodas(): Promise<any> {
    return this.http.get(this.usuarioUrl)
      .toPromise();
  }

   buscarPorId(id: number): Promise<Usuario> {
    const response = this.http.get<Usuario>(`${this.usuarioUrl}/${id}`)
      .toPromise();
    const usuario = response;
    return usuario;
    }



}

  import { TableModule } from 'primeng/table';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuarioRoutingModule } from './usuario-routing.module';
import { UsuarioPesquisaComponent } from './usuario-pesquisa/usuario-pesquisa.component';
import { UsuarioDetalheComponent } from './usuario-detalhe/usuario-detalhe.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [UsuarioPesquisaComponent, UsuarioDetalheComponent],
  imports: [
    CommonModule,
    UsuarioRoutingModule, 
    TableModule, 
    FormsModule
    
  ],
  exports:[
    UsuarioPesquisaComponent,
    UsuarioDetalheComponent,
    
  ]
})
export class UsuarioModule { }

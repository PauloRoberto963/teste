import { UsuarioService } from './../usuario.service';
import { Usuario } from './../../model/usuario';
import { Component, OnInit } from '@angular/core';
import { trigger,state,style,transition,animate } from '@angular/animations';



@Component({
  selector: 'app-usuario-pesquisa',
  templateUrl: './usuario-pesquisa.component.html',
  styleUrls: ['./usuario-pesquisa.component.css'],
  animations: [
    trigger('rowExpansionTrigger', [
        state('void', style({
            transform: 'translateX(-10%)',
            opacity: 0
        })),
        state('active', style({
            transform: 'translateX(0)',
            opacity: 1
        })),
        transition('* <=> *', animate('400ms cubic-bezier(0.86, 0, 0.07, 1)'))
    ])
]
})
export class UsuarioPesquisaComponent implements OnInit {

  usuarios: Usuario[];
  cols: any[];
  detalhes: any[]

  constructor(private usuarioService : UsuarioService) { }

  ngOnInit(): void {
   this.usuarioService.listarTodas().then(usuarios => this.usuarios = usuarios);
    this.cols = [
            { field: 'id', header: 'ID' },
            { field: 'name', header: 'Name' }       
        ];

  }

}

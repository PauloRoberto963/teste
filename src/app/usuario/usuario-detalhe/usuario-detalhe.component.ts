import { UsuarioService } from './../usuario.service';
import { Usuario } from './../../model/usuario';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usuario-detalhe',
  templateUrl: './usuario-detalhe.component.html',
  styleUrls: ['./usuario-detalhe.component.css']
})
export class UsuarioDetalheComponent implements OnInit {

  usuario = new Usuario();
  product_id: number;
  constructor(       
    
    private route: ActivatedRoute,
    private usuarioService : UsuarioService
    
    ) { 
      this.product_id = this.route.snapshot.params.id;

    }

  ngOnInit(): void {
    
    console.log(this.product_id);
    this.buscarUsuario(this.product_id);

  }

  buscarUsuario(id: number){
    console.log(id);
    this.usuarioService.buscarPorId(id)
    .then(usuario => {
      this.usuario = usuario;
    })
    .catch(erro => console.log(erro));
  }
}


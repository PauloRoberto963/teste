import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsuarioPesquisaComponent } from './usuario-pesquisa/usuario-pesquisa.component';
import { UsuarioDetalheComponent } from './usuario-detalhe/usuario-detalhe.component';


const routes: Routes = [

  {
    path: '',
    component: UsuarioPesquisaComponent
  },

  {
    path: ':id',
    component: UsuarioDetalheComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuarioRoutingModule { }
